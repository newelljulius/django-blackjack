import random


class GameEngine:
    def __init__(self):
        self.deck = self.get_new_deck()
        self.dealer_hand, self.player_hand = self.get_initial_hand()
        self.dealer_in = True
        self.player_in = True

    def get_new_deck(self):
        deck = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K'] * 4
        random.shuffle(deck)
        return deck

    def get_initial_hand(self):
        player_hand = [self.draw_card()]
        dealer_hand = ['A', 'A']  # TEST CASE GLITCH
        """
        dealer_hand = [self.draw_card(), self.draw_card()]
        """
        player_hand_display = [card if isinstance(card, int)
                               else 'A' for card in player_hand]
        dealer_hand_display = [card if isinstance(card, int)
                               else 'A' for card in dealer_hand]

        return player_hand_display, dealer_hand_display

    def calculate_card_value(self, card):
        if card == 'A':
            return 11
        elif card in ['K', 'Q', 'J']:
            return 10
        else:
            return int(card)

    def draw_card(self):
        if not self.deck:
            self.deck = self.get_new_deck()
        card = self.deck.pop()
        return card

    def total(self, hand):
        converted_hand = [card if isinstance(card, int)
                          else self.calculate_card_value(card)
                          for card in hand]
        if 11 in converted_hand and sum(converted_hand) > 21:
            converted_hand.remove(11)
            converted_hand.append(1)
        return sum(converted_hand)

    def deal_card(self, hand):
        hand.append(self.draw_card())

    def reveal_hand(self, hand):
        revealed_hand = hand[1:]
        displayed_hand = [card if isinstance(card, int)
                          else 'A' for card in revealed_hand]
        return displayed_hand

    def reset(self):
        self.deck = self.get_new_deck()
        self.dealer_hand, self.player_hand = self.get_initial_hand()
        self.dealer_in = True
        self.player_in = True

    def to_dict(self):
        return {
            'deck': self.deck,
            'player_hand': self.player_hand,
            'dealer_hand': self.dealer_hand,
            'player_in': self.player_in,
            'dealer_in': self.dealer_in,
        }

    @classmethod
    def from_dict(cls, game_dict):
        game = cls()
        game.deck = game_dict['deck']
        game.player_hand = game_dict['player_hand']
        game.dealer_hand = game_dict['dealer_hand']
        game.player_in = game_dict['player_in']
        game.dealer_in = game_dict['dealer_in']
        return game

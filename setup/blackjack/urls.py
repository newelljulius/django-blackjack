from django.urls import path
from .views import home
from . import views
from accounts.views import enter_wager

app_name = 'blackjack'

urlpatterns = [
    path('', home, name='home'),
    path('start_game/', views.start_game, name='start_game'),
    path('enter_wager/', enter_wager, name='enter_wager'),
]

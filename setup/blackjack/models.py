from django.db import models
from django.conf import settings
from .game_engine import GameEngine


# Create your models here.
class game_session(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="games",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name

    def play_game(self):
        game = GameEngine()
        game.play_game()

# Generated by Django 4.2.2 on 2023-06-13 17:00

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("blackjack", "0002_blackjackgame"),
    ]

    operations = [
        migrations.DeleteModel(
            name="BlackjackGame",
        ),
    ]

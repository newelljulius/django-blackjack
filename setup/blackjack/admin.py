from django.contrib import admin
from blackjack.models import game_session


# Register your models here.
@admin.register(game_session)
class GameSessionAdmin(admin.ModelAdmin):
    pass

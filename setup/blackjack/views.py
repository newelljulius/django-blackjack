from django.shortcuts import render
from .game_engine import GameEngine
from django.contrib.auth.decorators import login_required


@login_required
def home(request):
    return render(request, 'blackjack/home.html')


@login_required
def start_game(request):
    user = request.user
    account_balance = request.user.account.balance \
        if request.user.account else 0
    wager = request.POST.get('money')

    if wager and int(wager) <= account_balance:
        wager = int(wager)
        account_balance -= wager
        request.session['initial_wager'] = wager

    initial_wager = request.session.get('initial_wager')

    game_dict = request.session.get('game', None)

    if game_dict is None:
        game = GameEngine()
        game.get_initial_hand()
    else:
        game = GameEngine.from_dict(game_dict)

    context = {
        'player_hand': game.player_hand,
        'dealer_hand': game.dealer_hand,
        'player_sum': game.total(game.player_hand),
        'dealer_sum': game.total(game.dealer_hand),
        'result': None,
        'initial_wager': initial_wager,
        'winnings': initial_wager * 2,
        'new_balance': account_balance + (initial_wager * 2),
        'balance': account_balance,
    }

    if request.method == 'POST':
        action = request.POST.get('action')
        result = None

        if action == 'hit':
            game.deal_card(game.player_hand)
            context['player_sum'] = game.total(game.player_hand)
            player_total = game.total(game.player_hand)
            if player_total > 21:
                result = 'Dealer wins!'
        elif action == 'stay':
            game.player_in = False

            while game.dealer_in:
                dealer_total = game.total(game.dealer_hand)
                if dealer_total >= 17:
                    break
                game.deal_card(game.dealer_hand)
                context['dealer_sum'] = game.total(game.dealer_hand)

            player_total = game.total(game.player_hand)
            dealer_total = game.total(game.dealer_hand)

            if dealer_total > 21:
                result = 'You win!'
                winnings = initial_wager * 2
                account_balance += winnings
                user.account.balance = account_balance
                user.account.save()
            elif dealer_total == 21 and player_total == 21:
                result = "It's a tie!"
                winnings = initial_wager
                account_balance += winnings
                user.account.balance = account_balance
                user.account.save()
            elif player_total > dealer_total:
                result = 'You win!'
                winnings = initial_wager * 2
                account_balance += winnings
                user.account.balance = account_balance
                user.account.save()
            elif player_total < dealer_total:
                result = 'Dealer wins!'
            else:
                result = "It's a tie!"
                winnings = initial_wager
                account_balance += winnings
                user.account.balance = account_balance
                user.account.save()

        if result is not None:
            request.session.pop('game', None)
            request.session.pop('initial_wager', None)
            context['result'] = result
            context['balance'] = account_balance
            return render(request, 'blackjack/start_game.html', context)

    if user.account:
        user.account.balance = account_balance
        user.account.save()

    request.session['game'] = game.to_dict()
    return render(request, 'blackjack/start_game.html', context)

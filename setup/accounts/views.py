from django.shortcuts import render, redirect
from accounts.forms import Login_Form, SignUp_Form, AddCredits_Form
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.models import Currency, Account


# Create your views here.
def User_Login(request):
    if request.method == "POST":
        form = Login_Form(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = Login_Form()
    Currency.objects.get_or_create(name='USD')
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def User_Logout(request):
    logout(request)
    return redirect("login")


def User_SignUp(request):
    if request.method == "POST":
        form = SignUp_Form(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            confirm = form.cleaned_data["password_confirmation"]
            if password == confirm:
                user = User.objects.create_user(username, password=password)
                currency, _ = Currency.objects.get_or_create(name='USD')
                Account.objects.\
                    create(user=user, currency=currency, balance=0)
                login(request, user)
                return redirect("home")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignUp_Form()
    context = {"form": form}
    return render(request, "registration/signup.html", context)


def Add_Credits(request):
    if request.method == "POST":
        form = AddCredits_Form(request.POST)
        if form.is_valid():
            amount = form.cleaned_data['amount']
            account = request.user.account
            account.balance += amount
            account.save()
            return redirect('home')
    else:
        form = AddCredits_Form()

    context = {
        'form': form
    }
    return render(request, 'accounts/add_credits.html', context)


def enter_wager(request):
    if request.method == "POST":
        wager = int(request.POST.get("money"))

        account = request.user.account

        if account.balance >= wager:
            account.balance -= wager
            account.save()
            return redirect("start_game")
        else:
            print("Insufficient balance")

    return render(request, "accounts/home.html")

from django.contrib import admin
from accounts.models import Account


class AccountAdmin(admin.ModelAdmin):
    list_display = ('user', 'currency', 'balance')
    list_filter = ('currency',)


admin.site.register(Account, AccountAdmin)

from django.urls import path
from accounts.views import User_Login, User_Logout, User_SignUp, Add_Credits
from blackjack.views import start_game

urlpatterns = [
    path("login/", User_Login, name="login"),
    path("logout/", User_Logout, name="logout"),
    path("signup/", User_SignUp, name="signup"),
    path('add_credits/', Add_Credits, name='add_credits'),
    path("enter_wager/", start_game, name="enter_wager"),
    path("start_game/", start_game, name="start_game"),
]

from django import forms


class Login_Form(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )


class SignUp_Form(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )


class AddCredits_Form(forms.Form):
    amount = forms.DecimalField(max_digits=10, decimal_places=2, min_value=0)

    def clean_amount(self):
        amount = self.cleaned_data['amount']
        if amount <= 0:
            raise forms.ValidationError("Amount must be greater than zero.")
        if amount % 5 != 0:
            raise forms.ValidationError("Amount must be in increments of 5.")
        return amount
